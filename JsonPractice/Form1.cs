﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JsonPractice
{
    public partial class Form1 : Form
    {
        public Asmuo Asmuo { get; set; }
        public Form1()
        {
            InitializeComponent();
            Asmuo = new Asmuo
            {
                Vardas = "Jonas",
                Pavardė = "Jonaitis",
                TelNr = "+37069077649",
                Adresas = new Adresas
                {
                    Šalis = "Lietuva",
                    Miestas = "Vilnius",
                    Gatvė = "Netikima"

                }

            };

            var asmuoJson = JsonConvert.SerializeObject(Asmuo);
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
    public class Asmuo
    {
        public string Vardas { get; set; }
        public string Pavardė { get; set; }
        public string TelNr { get; set; }
        public Adresas Adresas { get; set; }
    }

    public class Adresas
    {
        public string Šalis { get; set; }
        public string Miestas { get; set; }
        public string Gatvė { get; set; }
    }
}
